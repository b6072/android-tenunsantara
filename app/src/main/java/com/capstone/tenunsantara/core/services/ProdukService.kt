package com.capstone.tenunsantara.core.services

import com.capstone.tenunsantara.core.ResponseData
import com.capstone.tenunsantara.core.models.Kategori
import com.capstone.tenunsantara.core.models.Produk
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Url

interface ProdukService {

    @GET
    fun kategoriStatic(@Url url: String): Observable<ResponseData<List<Kategori>>>

    @GET
    fun product(@Url url: String): Observable<ResponseData<List<Produk>>>

    @GET("products/{filter}")
    fun products(
        @Path("filter") filter: String
    ): Observable<ResponseData<List<Produk>>>

}