package com.capstone.tenunsantara.core.models

import com.google.gson.annotations.SerializedName

data class Produk(
    @SerializedName("id") var id: Int,

    @SerializedName("title") var title: String,

    @SerializedName("image") var image: String,

    @SerializedName("weaving") var weaving: Weaving,

    @SerializedName("desc") var desc: String,

    var favorite: Boolean = false,

    @SerializedName("price")
    val harga: String = "Rp 206.500"
) {
    data class Weaving(
        @SerializedName("id") var id: Int,
        @SerializedName("weaving_name") var weavingName: String,
        @SerializedName("weaving_etnik") var weavingEtnik: String,
        @SerializedName("weaving_category") var weavingCategory: String,
        @SerializedName("_links") var Links: Links
    )

    data class Links(
        @SerializedName("detail") var detail: String,
        @SerializedName("products_weaving") var productsWeaving: String,
        @SerializedName("products_weaving_category") var productsWeavingCategory: String,
        @SerializedName("wiki") var wiki: String
    )
}
