package com.capstone.tenunsantara.core.models

import com.google.gson.annotations.SerializedName

data class Kategori(
    @SerializedName("weaving_name")
    val name: String,

    @SerializedName("weaving_id")
    val id: Int,

    var active: Boolean = false
)

fun List<Kategori>.setDefault(): List<Kategori> {
    this[0].active = true
    return this
}

fun List<Kategori>.setReset(): List<Kategori> {
    forEach { it.active = false }
    return this
}
