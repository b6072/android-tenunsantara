package com.capstone.tenunsantara.core

import com.google.gson.annotations.SerializedName

data class ResponseData<T>(

    @SerializedName("response_code")
    val responseCode: String,

    @SerializedName("response_desc")
    val responseDesc: String,

    @SerializedName("data")
    val data: T

)
