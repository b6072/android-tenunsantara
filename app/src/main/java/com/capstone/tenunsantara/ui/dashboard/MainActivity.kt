package com.capstone.tenunsantara.ui.dashboard

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.capstone.tenunsantara.adapter.ListKategoriAdapter
import com.capstone.tenunsantara.adapter.ProdukAdapter
import com.capstone.tenunsantara.core.models.setDefault
import com.capstone.tenunsantara.core.services.ProdukService
import com.capstone.tenunsantara.databinding.ActivityMainBinding
import com.capstone.tenunsantara.ui.camera.CameraActivity
import com.capstone.tenunsantara.ui.detail.DetailActivity
import com.capstone.tenunsantara.utils.viewBinding
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityMainBinding::inflate)

    private val produkAdapter by lazy {
        ProdukAdapter()
    }

    private val kategoriAdapter by lazy {
        ListKategoriAdapter()
    }

    private val service: ProdukService by inject()

    private val disposable = CompositeDisposable()

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        ActivityCompat.requestPermissions(this@MainActivity,
            arrayOf(Manifest.permission.CAMERA), 102)

        binding.recyclerViewKategori.adapter = kategoriAdapter
        binding.recyclerViewProduk.adapter = produkAdapter
        binding.swipeRefresh.setOnRefreshListener {
            getData()
        }

        kategoriAdapter.setOnSelectListener { getData(it) }

        produkAdapter.setOnSelectListener { slug, image, title ->
            DetailActivity.navigate(this@MainActivity, slug, image, title)
        }

        disposable.add(service.kategoriStatic("https://naufalnibros.github.io/static/kategori.json")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                kategoriAdapter.kategori.clear()
                kategoriAdapter.kategori.addAll(it.data.setDefault())
                kategoriAdapter.notifyDataSetChanged()
            },
                {

                }, {

                }, {

                }
            ))

        getData()

        binding.imageScan.setOnClickListener {
            if (allPermissionsGranted()) {
                startActivity(Intent(this@MainActivity, CameraActivity::class.java))
            } else {
                requestPermission()
            }
        }

        binding.containerScan.setOnClickListener {
            startActivity(Intent(this@MainActivity, CameraActivity::class.java))
        }
    }

    private fun allPermissionsGranted() = arrayOf(
        Manifest.permission.CAMERA
    ).all {
        ContextCompat.checkSelfPermission(this@MainActivity, it) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this@MainActivity,
            arrayOf(
                Manifest.permission.CAMERA
            ),
            101
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 101) {
            if (!allPermissionsGranted()) {
                Toast.makeText(
                    this@MainActivity,
                    "Tidak mendapatkan permission.",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                startActivity(Intent(this@MainActivity, CameraActivity::class.java))
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun getData(tenun: String = "") {
        binding.swipeRefresh.isRefreshing = true
        disposable.add(service.products(if (TextUtils.isEmpty(tenun) || tenun.lowercase() == "semua") "" else "tenun-${tenun.lowercase()}")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                produkAdapter.list.clear()
                produkAdapter.list.addAll(it.data)
                produkAdapter.notifyDataSetChanged()

            },
                {
                    binding.swipeRefresh.isRefreshing = false
                }, {
                    binding.swipeRefresh.isRefreshing = false
                }, {

                }
            ))
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
    }
}