package com.capstone.tenunsantara.ui.detail

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.TextUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import com.bumptech.glide.Glide
import com.capstone.tenunsantara.adapter.ProdukRelatedAdapter
import com.capstone.tenunsantara.core.services.ProdukService
import com.capstone.tenunsantara.databinding.ActivityDetailBinding
import com.capstone.tenunsantara.utils.viewBinding
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject

class DetailActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityDetailBinding::inflate)

    private val disposable = CompositeDisposable()

    private val service: ProdukService by inject()

    private val produkAdapter by lazy {
        ProdukRelatedAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        binding.recyclerViewProduk.adapter = produkAdapter
        binding.containerBack.setOnClickListener { finish() }

        Glide.with(this)
            .load(intent.getStringExtra("preview"))
            .centerCrop()
            .into(binding.preview)

        produkAdapter.setOnSelectListener { slug, image, title ->
            navigate(this@DetailActivity, slug, image, title)
        }

        binding.title.text = intent.getStringExtra("title")

        binding.appCompatButton.setOnClickListener {
            CustomTabsIntent.Builder().build()
                .launchUrl(this@DetailActivity,
                    Uri.parse("https://www.tokopedia.com/ethnicapparelstore/kain-tenun-blanket-etnik-toraja-blh03"))
        }

        intent.getStringExtra("slug")?.let {
            disposable.add(service.product("https://capstone-project-c22-ps231.uc.r.appspot.com/api$it")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ resp ->
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        binding.content.text = Html.fromHtml(resp.data.first().desc, Html.FROM_HTML_MODE_LEGACY)
                    } else {
                        binding.content.text = Html.fromHtml(resp.data.first().desc)
                    }
                },
                    {

                    }, {

                    }, {

                    }
                )
            )
        }

        getData()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun getData() {
        disposable.add(service.products("")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                produkAdapter.list.clear()
                produkAdapter.list.addAll(it.data)
                produkAdapter.notifyDataSetChanged()
            },
                {

                }, {

                }, {

                }
            ))
    }

    companion object {
        fun navigate(context: Context, slug: String, image: String, title: String) {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra("slug", slug)
            intent.putExtra("preview", image)
            intent.putExtra("title", title)
            context.startActivity(intent)
        }
    }
}