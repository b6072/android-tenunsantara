package com.capstone.tenunsantara.ui.scan

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.capstone.tenunsantara.adapter.ProdukRelatedAdapter
import com.capstone.tenunsantara.core.services.ProdukService
import com.capstone.tenunsantara.databinding.ActivityScanBinding
import com.capstone.tenunsantara.ui.detail.DetailActivity
import com.capstone.tenunsantara.utils.Classifier
import com.capstone.tenunsantara.utils.viewBinding
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject
import java.io.File
import kotlin.math.floor


class ScanActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityScanBinding::inflate)

    private val service: ProdukService by inject()

    private val disposable = CompositeDisposable()

    private var prediction: String = ""

    private val produkAdapter by lazy {
        ProdukRelatedAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(binding.root)

        binding.recyclerViewProduk.adapter = produkAdapter

        produkAdapter.setOnSelectListener { slug, image, title ->
            DetailActivity.navigate(this@ScanActivity, slug, image, title)
        }

        binding.containerBack.setOnClickListener {
            finish()
        }

        //initializing data
        val pieEntries: ArrayList<PieEntry> = ArrayList()
        val label = "type"
        val typeAmountMap: MutableMap<String, Int> = HashMap()

        intent.getParcelableArrayListExtra<Classifier.Recognition>("recognitions")?.let {
            if (it.isNotEmpty()) {
                getData(it.first().title)
                prediction = it.first().title

                it.forEach { item ->
                    Log.d(localClassName, "onCreate: Classifier.Recognition=$item")
                    typeAmountMap["${item.confidence} ${item.title}"] = ((item.confidence.toDouble() / it.size) * 100).toInt()
                }
            } else {
                getData()
                typeAmountMap["Unknow"] = 100
            }
        }

        //initializing colors for the entries
        val colors: ArrayList<Int> = ArrayList()
        colors.add(Color.parseColor("#304567"))
        colors.add(Color.parseColor("#309967"))
        colors.add(Color.parseColor("#476567"))
        colors.add(Color.parseColor("#890567"))
        colors.add(Color.parseColor("#a35567"))

        //input data and fit data into pie chart entry
        for (type in typeAmountMap.keys) {
            pieEntries.add(PieEntry(typeAmountMap[type]!!.toFloat(), type))
        }

        val pieDataSet = PieDataSet(pieEntries, label)
        pieDataSet.valueTextSize = 12f
        pieDataSet.colors = colors
        val pieData = PieData(pieDataSet)
        pieData.setDrawValues(true)

        binding.chart.data = pieData
        binding.chart.invalidate()

        Glide.with(this)
            .load((intent.getSerializableExtra("picture") as File))
            .centerCrop()
            .into(binding.preview)

        binding.labelRelateProduk.text = "Temukan produk Tenun $prediction lainnya"

    }

    @SuppressLint("NotifyDataSetChanged")
    fun getData(tenun: String = "") {
        disposable.add(service.products(if (TextUtils.isEmpty(tenun) || tenun.lowercase() == "semua") "" else "tenun-${tenun.lowercase()}")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                produkAdapter.list.clear()
                produkAdapter.list.addAll(it.data)
                produkAdapter.notifyDataSetChanged()

            },
                {

                }, {

                }, {

                }
            ))
    }

    companion object {
        fun navigate(activity: Activity, recognitions: ArrayList<Classifier.Recognition>, picture: File) {
            val intent = Intent(activity, ScanActivity::class.java)
            intent.putExtra("picture", picture)
            intent.putParcelableArrayListExtra("recognitions", recognitions)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            activity.startActivity(intent)
            activity.finish()
        }
    }
}