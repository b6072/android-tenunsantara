package com.capstone.tenunsantara.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.capstone.tenunsantara.R
import com.capstone.tenunsantara.core.models.Produk
import com.capstone.tenunsantara.databinding.ItemProdukBinding
import com.capstone.tenunsantara.databinding.ItemProdukRelatedBinding

class ProdukRelatedAdapter(val list: MutableList<Produk> = mutableListOf()): RecyclerView.Adapter<ProdukRelatedAdapter.ViewHolder>() {

    private var onSelectListener: ((String, String, String) -> Unit)? = null

    fun setOnSelectListener(onSelect: (String, String, String) -> Unit) {
        this.onSelectListener = onSelect
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemProdukRelatedBinding.inflate(LayoutInflater.from(parent.context))
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(list[position]) {
            holder.binding.title.text = title
            holder.binding.harga.text = harga ?: "Rp 206.500"

            holder.binding.root.setOnClickListener {
                onSelectListener?.invoke(list[position].weaving.Links.detail, list[position].image, list[position].title)
            }

            Glide.with(holder.binding.root.context)
                .load(image)
                .centerCrop()
                .into(holder.binding.produkImageView)
        }
    }

    override fun getItemCount(): Int = list.size

    class ViewHolder(val binding: ItemProdukRelatedBinding)
        : RecyclerView.ViewHolder(binding.root)
}