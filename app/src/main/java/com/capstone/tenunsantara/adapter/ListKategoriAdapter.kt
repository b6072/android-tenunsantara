package com.capstone.tenunsantara.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.capstone.tenunsantara.R
import com.capstone.tenunsantara.core.models.Kategori
import com.capstone.tenunsantara.core.models.setReset
import com.capstone.tenunsantara.databinding.ItemKategoriBinding

class ListKategoriAdapter(val kategori: MutableList<Kategori> = mutableListOf()) : RecyclerView.Adapter<KategoriHolder>() {

    private var onSelectListener: ((String) -> Unit)? = null

    fun setOnSelectListener(select: (String) -> Unit) {
        this.onSelectListener = select
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): KategoriHolder {
        return KategoriHolder(ItemKategoriBinding.inflate(LayoutInflater.from(viewGroup.context)))
    }

    override fun getItemCount(): Int = kategori.size

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: KategoriHolder, position: Int) {
        holder.view.buttonKategori.text = kategori[position].name

        holder.view.root.setOnClickListener {
            kategori.setReset()
            kategori[position].active = true
            onSelectListener?.invoke(kategori[position].name)
            notifyDataSetChanged()
        }

        holder.view.buttonKategori.setColorText(
            if (kategori[position].active) {
                R.color.white
            } else {
                R.color.color_primary
            }
        )

        holder.view.container.settBackground(
            if (kategori[position].active) {
                R.color.color_primary
            } else {
                R.color.white
            }
        )
    }

    fun CardView.settBackground(@ColorRes color: Int) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            setCardBackgroundColor(resources.getColor(color, null))
        } else {
            setCardBackgroundColor((ContextCompat.getColor(context, color)))
        }
    }

    fun TextView.setColorText(@ColorRes textColor: Int) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            this.setTextColor(resources.getColor(textColor, null))
        } else {
            this.setTextColor(ContextCompat.getColor(context, textColor))
        }
    }

}

class KategoriHolder(val view: ItemKategoriBinding) : RecyclerView.ViewHolder(view.root)